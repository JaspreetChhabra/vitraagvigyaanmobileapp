// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

(function(){
  
  angular.module('starter.services', []);
  
  var app = angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers','starter.services','uiGmapgoogle-maps','angularMoment', 'cordova'])
  
  app.filter('fromNow', function() {
    return function(date) {
      return moment(date).startOf('sec').fromNow();
    }
  })


  app.filter('trusted', ['$sce', function ($sce) {
      return function(url) {
          return $sce.trustAsResourceUrl(url);
      };
  }])

  app.filter('externalLinks', function() {
   return function(text) {
     return String(text).replace(/href=/gm, "class=\"ex-link\" href=");
   }
  });

  app.config(function($cordovaInAppBrowserProvider){
      var defaultOptions = {
        location: 'no',
        clearcache: 'no',
        toolbar: 'yes'
      };

      document.addEventListener("deviceready", function () {

        $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions)

      }, false);
  });

      // app.constant('API_URL' , 'http://localhost/Projects/vitraagvigyaan/api/');
      // app.constant('IMAGE_URL' , 'http://localhost/Projects/vitraagvigyaan');
      //app.constant('API_URL' , 'http://actonatepanel.com/vv1/api/');
      //app.constant('IMAGE_URL' , 'http://actonatepanel.com/vv1');

      app.constant('API_URL' , 'http://vitraagvigyaan.org/api/');
      app.constant('IMAGE_URL' , 'http://vitraagvigyaan.org');


  app.run(function($ionicPlatform, push, $cordovaCalendar, $cordovaGeolocation, $localstorage, $rootScope, $state, $ionicLoading, AuthService,$cordovaNetwork, $http, API_URL, Toast) {
    $ionicPlatform.ready(function() {

      //$('.ex-link').click(function () {
      $(document).on('click', '.ex-link', function() {
       var url = $(this).attr('href');
       window.open(encodeURI(url), '_system', 'location=yes');
       return false;
      });
      
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      //Register the push notification
        // if(window.cordova){
        //     console.log("registering!");
        //     var result = push.registerPush(function (result) {
        //       alert('aaya');
        //         if (result.type === 'registration') {
        //             var device_id = result.id;
        //             var device = result.device;

        //             var user = { device_id: device_id, device: device };
        //             conosole.log(user);
        //             $http.post(API_URL + 'insert_deviceid', user)
        //             .success(function(data, status, headers, config) {
        //                 if(data.status=="success"){
        //                     Toast.show('Device registered with the server.')
        //                 }
        //                 else{
        //                     Toast.show(data.message, 'Push registration failed.', true);
        //                 }
        //             }).
        //             error(function(data, status, headers, config) {
        //                 Toast.show('app-error', 'Push registration caused an exception on the server.', true);
        //             });
        //         }
        //     });
        // }

    if(window.cordova){
     var self = this;
     console.log('registering');
      var result = push.registerPush(function (result) {
          if (result.type === 'registration') {
              var device_id = result.id;
              var device = result.device;

              var user = { device_id: device_id, device: device };

              $http.post(API_URL + 'insert_deviceid', user)
              .success(function(data, status, headers, config) {
                  if(data.status=="success"){
                      //Toast.show('Device registered with the server.')
                  }
                  else{
                      Toast.show(data.message, 'Push registration failed.', true);
                  }
              }).
              error(function(data, status, headers, config) {
              });
          }
      });
  }

      if(angular.equals($localstorage.getObject('session'), {})){
        $rootScope.logged_in = false;
        $state.go('app.Events');
      }
      else{
          $rootScope.logged_in = true;
          AuthService.authenticate_session($localstorage.getObject('session'));
      }

      

      //Check authentication
      // if(!angular.equals($localstorage.getObject('session'), {})) {
      //       var session = $localstorage.getObject('session');
      //       // AuthService.authenticate_session(session);
            
      //     }
      //     else{
      //       console.log(session);
            
      //     }

      //Setup the loading and hiding actions
      $rootScope.$on('loading:show', function() {
        $ionicLoading.show({template: 'Loading...'})
      });

      $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide();
      });


    });
  })

  // app.constant('API_URL' , 'http://localhost/Projects/vitraagvigyaan/api/');
  // app.constant('IMAGE_URL' , 'http://localhost/Projects/vitraagvigyaan');
  //app.constant('API_URL' , 'http://actonatepanel.com/vv1/api/');
  //app.constant('IMAGE_URL' , 'http://actonatepanel.com/vv1');

  app.constant('API_URL' , 'http://vitraagvigyaan.org/api/');
  app.constant('IMAGE_URL' , 'http://vitraagvigyaan.org');
  
  app.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyDuV3jqOBH2tx_wjXaXSaWKqwQSMTdQ4MY',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
  });

    //Interceptor to show $ionicLoading
  app.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($rootScope,API_URL) {
      return {
        request: function(config) {
          if(config.url != API_URL + "satsang_announcements"){
             $rootScope.$broadcast('loading:show')
          }
          return config;
        },
        response: function(response) {
          $rootScope.$broadcast('loading:hide')
          return response;
        },
        requestError: function(rejection){
          $rootScope.$broadcast('loading:hide')
          return rejection;
        },
        responseError: function (rejection) {
          $rootScope.$broadcast('loading:hide')
          return rejection;
        }
      }
    });
  })


  app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })

    .state('app.knowUs', {
      url: "/knowUs",
      views: {
        'menuContent': {
          templateUrl: "templates/KnowUs/knowUs.tpl.html"
        }
      }
    })

    .state('app.ContactUs', {
      url: "/ContactUs",
      views: {
        'menuContent': {
          templateUrl: "templates/ContactUs/contactUs.tpl.html",
          controller: 'ContactUsCtrl'
        }
      }
    })

    .state('app.register', {
      url: "/register",
      views: {
        'menuContent': {
          templateUrl: "templates/Auth/register.tpl.html",
          controller: 'RegisterCtrl'
        }
      }
    })

    .state('app.forgotPassword', {
      url: "/register",
      views: {
        'menuContent': {
          templateUrl: "templates/Auth/forgot_password.tpl.html",
          controller: 'ForgotPasswordCtrl'
        }
      }
    })

    .state('app.login', {
      url: "/login",
      views: {
        'menuContent': {
          templateUrl: "templates/Auth/login.tpl.html",
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.News', {
        url: "/News",
        views: {
          'menuContent': {
            templateUrl: "templates/News/news-list.tpl.html",
            controller: 'NewsCtrl'
          }
        }
      })

      .state('app.Events', {
        url: "/Events",
        views: {
          'menuContent': {
            templateUrl: "templates/Events/event_list.tpl.html",
            controller: 'EventCtrl'
          }
        }
      })

      .state('app.UpcomingEvents', {
        url: "/UpcomingEvents",
        views: {
          'menuContent': {
            templateUrl: "templates/Events/upcomingevent_list.tpl.html",
            controller: 'UpcomingEventCtrl'
          }
        }
      })

      .state('app.PastEvents', {
        url: "/PastEvents",
        views: {
          'menuContent': {
            templateUrl: "templates/Events/pastevent_list.tpl.html",
            controller: 'PastEventCtrl'
          }
        }
      })

      .state('app.EventDetails', {
       url: "/EventDetails/:eventId",
        views: {
          'menuContent': {
            templateUrl: "templates/Events/event_details.tpl.html",
            controller: 'EventDetailCtrl'
          }
        }
      })

      .state('app.maps', {
       url: "/maps/:lat/:lng/:name",
        views: {
          'menuContent': {
            templateUrl: "templates/map.tpl.html",
            controller: 'MapCtrl'
          }
        }
      })

    .state('app.pastSatsang', {
      url: "/pastSatsang",
      views: {
        'menuContent': {
          templateUrl: "templates/Satsang/pastSatsang.tpl.html",
          controller: 'PastSatsangCtrl'
        }
      }
    })

    .state('app.satsang', {
      url: "/satsang/:type/:id?",
      views: {
        'menuContent': {
          templateUrl: "templates/Satsang/satsang.tpl.html",
          controller: 'SatsangCtrl'
        }
      }
    })

    .state('app.VideoDetails', {
      url: "/VideoDetails/:id",
      views: {
        'menuContent': {
          templateUrl: "templates/Video/Video-details.tpl.html",
          controller: 'VideoDetailsCtrl'
        }
      }
    })

    .state('app.Video', {
      url: "/Video",
      views: {
        'menuContent': {
          templateUrl: "templates/Video/Video-list.tpl.html",
          controller: 'VideoCtrl'
        }
      }
    })

    .state('app.Ebook', {
      url: "/Ebook",
      views: {
        'menuContent': {
          templateUrl: "templates/Ebook/Ebook.tpl.html",
          controller: 'EbookCtrl'
        }
      }
    })

    .state('app.notloggedin', {
      url: "/notloggedin",
      views: {
        'menuContent': {
          templateUrl: "templates/Auth/notloggedin.tpl.html",
          controller: 'NotLoggedInCtrl'
        }
      }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/Events');

  });
})();
