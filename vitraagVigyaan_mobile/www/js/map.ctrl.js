(function(){
	var app = angular.module('starter.controllers');
	//Global MapController

   // .filter('split', function() {
   //      return function(input, splitChar, splitIndex) {
   //          // do some bounds checking here to ensure it has that index
   //          return input.split(splitChar)[splitIndex];
   //      }
   //  });

  app.controller("MapCtrl", function($scope, uiGmapGoogleMapApi ,$stateParams , $http, $location) {

         //Initialize the Google Map
         //$scope.map = { center: { latitude: $scope.lat, longitude: $scope.lon }, zoom: 7 };

        

        

    uiGmapGoogleMapApi.then(function(maps) {
      
      $scope.back = function(){
          console.log("hello!");
          $ionicHistory.goBack();
        }
        
      $scope.map = { center: { latitude:$stateParams.lat, longitude:$stateParams.lng}, zoom: 8 };

      $scope.marker = {
          id: 0,
          coords: {
            latitude:  $stateParams.lat,
            longitude:  $stateParams.lng
          },
          options: { 
            draggable: false,
            labelContent: '<h5>' + $stateParams.name + '</h5>',
            labelClass: 'google-map-marker-label'
          },
          events: {
          }
        };
    });
});

})();
   