(function(){
	var app=angular.module('starter.controllers');

	app.controller("RegisterCtrl", function($scope, $http ,$stateParams, $state , AuthService, $q , IMAGE_URL ,$ionicLoading ,$ionicPopup, $timeout, Toast){
		  
    $scope.user = {};
    //$scope.email1 = "/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/"

    var showAlert = function(message) {
      var alertPopup = $ionicPopup.alert({
         title: 'Please Note',
         template: message
       });
       alertPopup.then(function(res) {
         console.log(res);
       });
    };

    
    //registers user     
    $scope.register = function() { 	
        var user = $scope.user;
        console.log(user);
        AuthService.register(user, function(err, data){
            Toast.show(data.message);
            if(data.status === "success"){
              $state.go('app.Events');
            }
        });
    };

        
	});
})();

