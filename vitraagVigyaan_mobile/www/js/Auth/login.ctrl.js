(function(){
    var app = angular.module('starter.controllers');
    app.controller('LoginCtrl', function($scope,AuthService,$stateParams, $state, $http, API_URL,$ionicLoading , $localstorage, Toast, $cordovaOauth) {


        if(angular.equals($localstorage.getObject('session'), {})){
            //$rootScope.logged_in = false;
            $state.go('app.login');
        }else
        {
            //$rootScope.logged_in = true;
            $state.go('app.knowUs');
        }

        $scope.user = {};

        //link to registeration page of user and dealer
        $scope.registrationpagelink = function() {  
            $state.go('app.register');
        }

        //user logs in to the application through username and password
        $scope.login = function() {
            var user = $scope.user;
            console.log($scope.user);
            
            $http.post(API_URL + 'login', user).
            success(function(data, status, headers, config) {
                console.log(data);
                if(data.status=="success"){
                    var user_data = data.data;
                    console.log('Login Response' + data.data);
                    AuthService.authenticate_session(user_data);
                }
                else{
                    //Toast.show(data.message);
                    Toast.show(data.message);
                    $scope.user={};
                }
                 //$state.go('app.knowUs');
            }).
            error(function(data, status, headers, config) {
                Toast.show("app-error");
                $scope.user = {};
            });
        };

        //User signs in to the application using FACEBOOK authentication
        $scope.signin_google = function() {
            $cordovaOauth.google("172297222633-avp1euanm3tm43rece3fm165cmgm3p0o.apps.googleusercontent.com", ["profile","email"]).then(function(result) {
                AuthService.authenticate_google_user(result.access_token, function(err, message){
                    if(err){
                        Toast.show(message);
                        return;
                    }
                    Toast.show(message);
                });

            }, function(error) {
                Toast.show("app-error")
            });
        }
        
        $scope.signin_facebook = function() {
          $cordovaOauth.facebook("828753693845407", ["email","public_profile"]).then(function(result) {
                AuthService.authenticate_facebook_user(result.access_token, function(err, message){
                    if(err){
                        Toast.show(message);
                        return;
                    }
                    Toast.show(message);
                });

            }, function(error) {
                Toast.show("Sorry an error occurred. Please contact the application administrator.", 'long', 'bottom')
            });
        };
    });
})();