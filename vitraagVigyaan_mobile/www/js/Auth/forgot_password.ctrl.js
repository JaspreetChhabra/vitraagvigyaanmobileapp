
(function(){
    var app = angular.module('starter.controllers');
    app.controller('ForgotPasswordCtrl', function($scope,$stateParams, $state, $http, API_URL,$ionicLoading ,Toast) {

        $scope.user = {};

        //user logs in to the application through username and password
        $scope.send_mail = function() {
            var user = $scope.user;
            console.log(user);

            $http.post(API_URL + 'forgot_password', user).
	            success(function(data, status, headers, config) {

	                response = data;
	               

	                if(response == '' || response == null){
						Toast.show('Sorry, an error occured.');
	      				return;
					}

					if(response.status === "error" && response.data === 1){
	      				Toast.show(response.message);
	      				return;
	      			}
	      			
	      			if(response.status === "success" && response.data === 0){
						$scope.user = {};				
	      				Toast.show(response.message);
	      				return;
	      			}

	                
	            }).
	            error(function(data, status, headers, config) {
	                Toast.show("app-error");
	                $scope.user = {};
	            });
        };
     });
})();