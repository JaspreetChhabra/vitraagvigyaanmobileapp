(function(){

	var app = angular.module('starter.controllers');
	app.controller('AppCtrl', function($scope, $timeout, sideMenuService, $window, $ionicHistory, $http, $state,$localstorage,AuthService) {
        
        $timeout(function(){
            sideMenuService.openSideMenu('sideMenu');
        }, 4000);
        //Logout Functionality
        $scope.logout = function(){
            //console.log($localstorage.getObject('session'));
            $localstorage.remove('session');

            $ionicHistory.nextViewOptions({
               disableBack: true
            });
            $window.location.hash = "";
            $window.location.reload(true)
        }


        
    });
})();