(function(){

	var app = angular.module('starter.services');

	app.service('AuthService',function($http , API_URL,$localstorage,$ionicHistory,$state,$window, push, Toast, $rootScope){

		var self=this; 

    var session = { member: [] };

		var	urlbase = API_URL;

    var authenticate_facebook_user = function(access_token, callback){
        $http.get('https://graph.facebook.com/v2.2/me?access_token=' + access_token).
        success(function(data, status, headers, config) {
    
            var first_name = data.first_name;
            var last_name = data.last_name;

            var email = data.email;
            
            var user_obj = {first_name: first_name, last_name: last_name, email: email, is_facebook: true};

            $http.post(API_URL + 'callback', user_obj).
            success(function(data, status, headers, config) {
                if(data.status === "success" && data.data === "[approval]"){
                    callback(false, data.message);
                    return;
                }
                else if(data.status === "success"){
                    var user = data.data;
                    authenticate_session(user);
                }
                else{
                    callback(true, data.message);
                    //window.analytics.trackException('Facebook sign in could not be completed by the callback', true);
                }

            }).
            error(function(data) {
                alert("Sorry your sign in couldn't be completed. Please try again later.");
                //window.analytics.trackException('Facebook sign in could not be completed by the callback.', true);
            });

        }).
        error(function(data, status, headers, config) {
            alert("Sorry couldn't access your Facebook. Please contact VehkL support via amdigon.com.");
            //window.analytics.trackException('Facebook sign in error with the facebook sdk.', true);
        });
    };

    var authenticate_google_user = function(access_token, callback){

        var config = {
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'Content-Type': 'application/json'
            }
        };

        $http.get('https://www.googleapis.com/plus/v1/people/me', config).
        success(function(data, status, headers, config) {
            if(data.name.givenName === undefined || data.emails === undefined || data.emails.length === 0){
                callback(true, "Sorry couldn't access your Google+ profile. If you are using Google Apps for Work, your administrator might have disabled Sign in with Google.");
                //window.analytics.trackException('Google+ login couldnt be completed, possible SDK problem.', false);
                return;
            }

            var first_name = data.name.givenName;
            var last_name = data.name.familyName;

            if(data.name.familyName === undefined){
                last_name = "";
            }

            var email = data.emails[0].value;
            
            var user_obj = {first_name: first_name, last_name: last_name, email: email, is_google: true };

            $http.post(API_URL + 'callback', user_obj).
            success(function(data, status, headers, config) {
                if(data.status === "success" && data.data === "[approval]"){
                    callback(false, data.message);
                    return;
                }
                else if(data.status === "success"){
                    var user = data.data;
                    authenticate_session(user);

                    //success
                    callback(false, "logged in successfully.");
                }
                else{
                    callback(true, data.message);
                    //window.analytics.trackException('Google+ login couldnt be completed, problem with the callback.', true);
                }

            }).
            error(function(data) {
                callback(true, "Sorry couldn't access your Google+ profile. If you are using Google Apps for Work, your administrator might have disabled Sign in with Google.");
                //window.analytics.trackException('Google+ login couldnt be completed, problem with the callback.', true);
            });

        }).
        error(function(data, status, headers, config) {
            alert("Sorry couldn't access your Google+ profile. If you are using Google Apps for Work, your administrator might have disabled Sign in with Google.");
            //window.analytics.trackException('Google+ login couldnt be completed, possible SDK problem.', false);
        });
    };

    var authenticate_session = function (user,navigate){
        //Check if the Car array exists or not
        // if(user.Car == undefined){
        //     user.Car = [];
        // }

        $localstorage.setObject('session', user);
        
        $http.defaults.headers.common.session_id = user.session;

        //Copy to the session variable
        angular.extend(session, user);

        $rootScope.logged_in = true;
        
        if(navigate || navigate == undefined){
            $ionicHistory.nextViewOptions({
               disableBack: true
            });

            $state.go('app.Events');
        }


        //Register the push notification
        if(window.cordova){
            console.log("registering!");
            var result = push.registerPush(function (result) {
                if (result.type === 'registration') {
                    var device_id = result.id;
                    var device = result.device;

                    var user = { device_id: device_id, device: device };

                    $http.post(API_URL + 'update_deviceid', user)
                    .success(function(data, status, headers, config) {
                        if(data.status=="success"){
                            //Toast.show('Device registered with the server.')
                        }
                        else{
                            Toast.show(data.message, 'Push registration failed.', true);
                        }
                    }).
                    error(function(data, status, headers, config) {
                        Toast.show('app-error', 'Push registration caused an exception on the server.', true);
                    });
                }
            });
        }
    }
          

      var register = function(user, callback){
        console.log("register in service");   
      
        $http.post(urlbase+'register', user).
        success(function(response, status, headers, config) {
        	// var success = response.data;
           
          if(status=="success"){
            callback(false, response);
                // if(response.data = "success"){
                //   response.data = "Member registered";
                // }else if(response.data = "exist"){
                //   response.data = "Member Exist";
                // }else
                // {
                //   response.data = "Member Request Not send ";
                // }
            }
            else{
                callback(true, response);
            }

		}).
        error(function(data, status, headers, config) {
                    callback(true, "API Error");
        });
      };

	return {
			register: register,
            authenticate_session:authenticate_session,
            authenticate_facebook_user: authenticate_facebook_user,
            authenticate_google_user: authenticate_google_user
		};

	});
})();