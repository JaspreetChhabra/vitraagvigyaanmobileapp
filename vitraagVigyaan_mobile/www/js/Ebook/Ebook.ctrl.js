(function(){
  var app=angular.module('starter.controllers');

  app.controller("EbookCtrl", function(IMAGE_URL,$scope, $q, $http ,$cordovaGeolocation, EbookService , IMAGE_URL, Toast){

    var self=this; 

    //image url
    $scope.image_url = IMAGE_URL;

    $scope.ebook_container = "/js/ViewerJS/#";

    $scope.ebook =[];
    
    $scope.loaded = false;

      $scope.getEbooks = function(mode){
        

      var success_callback = function(response){
        response = response.data;
        console.log(response);
        $scope.ebook =[];

        $scope.loaded = true;

            if(response == '' || response == null){
              Toast.show('Sorry, an error occured.');
                  return;
            }

            if(response.status === "error"){
              Toast.show(response.message);
              return;
            }
            
            if(response.status === "success" && response.data === "[zero]"){
              $scope.ebook = [];              
              Toast.show(response.message);
              return;
            }

          $scope.ebook = response.data;
          $scope.loaded = true;
        }

        var error_callback = function(err){
          Toast.show('api-error');
          $scope.loaded = true;
        }

        $scope.loaded = false;
        $scope.ebook = [];
        EbookService.getEbooks().then(success_callback, error_callback);

      }

      $scope.getEbooks();
    
  });
})();

