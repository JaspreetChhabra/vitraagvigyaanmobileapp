(function(){

	var app = angular.module('starter.services');

	app.service('EbookService',function($http , API_URL){

		var self=this; 

		var	urlbase = API_URL;

	var getEbooks = function(){
			return $http.post(urlbase+'getAllEbook');
    };
    
	return {
			 getEbooks: getEbooks,
		};

	});
})();