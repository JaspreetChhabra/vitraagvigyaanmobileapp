(function(){
	var app = angular.module('starter.services');
 
	app.factory('$localstorage', ['$window', function($window) {
	  return {
	    set: function(key, value) {
	      $window.localStorage[key] = value;
	    },
	    get: function(key, defaultValue) {
	      return $window.localStorage[key] || defaultValue;
	    },
	    remove: function(key){
	    	delete $window.localStorage[key];
	    	return;
	    },
	    setObject: function(key, value) {
	      $window.localStorage[key] = JSON.stringify(value);
	    },
	    getObject: function(key) {
	      return JSON.parse($window.localStorage[key] || '{}');
	    }
	  }
	}]);

	app.service('sideMenuService', function($ionicSideMenuDelegate) {
	    return {
	        openSideMenu: function(menuhandle) 
	        {
	            console.log('open menu');
	            return $ionicSideMenuDelegate.$getByHandle(menuhandle).toggleLeft();
	        }
	    }
	})

	app.factory('Toast', function($cordovaToast){
		return {
			show: function(message, reason, fatal){
				if(message === 'app-error'){
					message = "Sorry an error occurred. Please contact the application administrator at info@amdigon.com.";
					if(reason == undefined){
						reason = "";
					}
					if(fatal == undefined){
						fatal = false;
					}
					window.analytics.trackException('[app-error]:' + reason, fatal);
				}
				else if(message === 'api-error'){
					message = "Sorry a server error occurred. Please contact the application administrator at info@amdigon.com.";
					if(reason == undefined){
						reason = "";
					}
					if(fatal == undefined){
						fatal = false;
					}
					window.analytics.trackException('[app-error]:' + reason, fatal);
				}

				if(window.cordova === undefined){
					console.log(message);
				}
				else{
					$cordovaToast.show(message, 'long', 'bottom');
				}
			}
		}
	});
	
})();