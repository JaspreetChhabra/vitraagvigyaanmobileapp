(function(){
	var app=angular.module('starter.controllers');

	app.controller("EventCtrl", function($scope, eventService, $q, $http ,$cordovaGeolocation, CityService , IMAGE_URL, Toast){

		var self=this; 

		//image url
		$scope.image_url = IMAGE_URL;

		$scope.eventlist =[];
		
		$scope.selection = 'current';

		$scope.loaded = false;

    	$scope.switchEvents = function(mode){
    		$scope.selection = mode;

			var success_callback = function(response){
				response = response.data;
				$scope.eventlist =[];

				$scope.loaded = true;

				if(response == '' || response == null){
					Toast.show('Sorry, an error occured.');
      				return;
				}

				if(response.status === "error"){
      				Toast.show(response.message);
      				return;
      			}
      			
      			if(response.status === "success" && response.data === "[zero]"){
					$scope.eventlist = [];      				
      				Toast.show(response.message);
      				return;
      			}

				$scope.eventlist = response.data;

			}

			var error_callback = function(err){
				Toast.show('api-error');
				$scope.loaded = true;
			}

			$scope.loaded = false;
			$scope.eventlist = [];

			if(mode === 'current'){
				eventService.getEvents().then(success_callback, error_callback);	
			}
			else if(mode == 'upcoming'){
				eventService.getEvents_upcoming().then(success_callback, error_callback);		
			}
			else if(mode == 'past'){
				eventService.getEvents_past().then(success_callback, error_callback);		
			}
			else{
				eventService.getEvents().then(success_callback, error_callback);	
			}
    	}

    	$scope.switchEvents('current');
    
	});
})();

