(function(){
	var app=angular.module('starter.controllers');

	app.controller("EventDetailCtrl", function($scope, $timeout, $http ,$stateParams, eventService, $q, $ionicSlideBoxDelegate, IMAGE_URL, Toast, $cordovaCalendar){

		$scope.loaded = false;
		console.log($stateParams);
		eventService.getEventDetails($stateParams.eventId).then(function(response){
			// console.log("stateParams" , $stateParams)
			$scope.loaded = true;

			if(response.data == '' || response.data == null){
				$scope.loaded = false;
				Toast.show('Sorry, an error occured. Please try again later.');
  				return;
			}

			$scope.eventlist = response.data;

		console.log("Controller Event Details" , $scope.eventlist);
		}, function(err){
			$scope.error = true;
			Toast.show('Sorry, an error occured');
		});

		$scope.myActiveSlide =1;
		$scope.image_url = IMAGE_URL;

		$scope.addEvent = function(event){

		  if(event.to_date === '' || event.to_date == null){
		  	event.to_date1 = event.from_date;
		  }
		  else{
		  	event.to_date1 = event.to_date;
		  }
		  console.log(event.to_date1);
	      $cordovaCalendar.createEvent({
		        title: event.name,
		        location: event.city_name+', '+event.state_name,
		        notes: event.shortdes,
		        startDate: event.from_date,
		        endDate: event.to_date1
		      }).then(function (result) {
		        console.log('added' , result);
		        Toast.show('Event added to your calendar');
		      }, function (err) {
		        console.log('error' , result);
		        Toast.show('Error adding event');
	      });

		}
	});
})();

