(function(){

	var app = angular.module('starter.services');

	app.service('CityService',function($http , API_URL){

		var self=this; 

		var	urlbase = API_URL;

		var getCityList = function(){
			return $http.get(urlbase  + 'getCities')
    };

    var getCity = function(latlng){
      console.log(latlng);
		  return $http.post(urlbase+'geocode', {'message' : latlng});
    }
 
   return {
			getCityList: getCityList,
			getCity: getCity
		};

	});
})();