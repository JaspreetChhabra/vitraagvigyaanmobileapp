(function(){

	var app = angular.module('starter.services');

	app.service('eventService',function($http , API_URL){

		var self=this; 

		var	urlbase = API_URL;

	var getEvents = function(){
			return $http.post(urlbase+'getEvents');
    };

   //  var getCityEvents = function(){
			// return $http.get(urlbase+'getEvents');
   //  };

    var getEvents_upcoming = function(){
      return $http.post(urlbase+'getUpcomingEvents');
    };

    var getEvents_past = function(){
      return $http.post(urlbase+'getPastEvents')
    };

    var getEventDetails = function(event_id){
			return $http.post(urlbase+'getEventDetails', { id: event_id});
    };
    
	return {
			 getEvents: getEvents,
       		 getEvents_upcoming: getEvents_upcoming,
       		 getEvents_past: getEvents_past,
			 getEventDetails: getEventDetails
		};

	});
})();