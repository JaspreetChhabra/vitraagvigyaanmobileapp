(function(){
	var app=angular.module('starter.controllers');

	app.controller("PastEventCtrl", function($scope, eventService, $q, $http ,$cordovaGeolocation,CityService , IMAGE_URL){

		var self=this; 

		//image url 
		$scope.image_url = IMAGE_URL;

		$scope.eventlist =[];
		$scope.update = function(selectedItem1){
    	console.log("called" , selectedItem1);
    		eventService.getCityEvents_past(selectedItem1).then(function(response){
			console.log("Selected City" , selectedItem1);
			$scope.eventlist =[];
			$scope.eventlist = response.data;
			
		    console.log("Controller Events " , $scope.eventlist);
		}, function(err){
			console.log(err);
		});
    	};

		CityService.getCityList().then(function(response){
			
			$scope.citylist = response.data;

		console.log("Controller Cities" , $scope.citylist);
		}, function(err){
			console.log(err);
		});

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    $cordovaGeolocation
    .getCurrentPosition(posOptions)
    	.then(function (position) {
	      var lat  = position.coords.latitude
	      var longi = position.coords.longitude
	      var latlng = lat+','+longi;
     	  console.log("GeoLocation" , latlng);

          CityService.getCity(latlng).then(function(response){
	      	$scope.city = response.data;
	      	$scope.selectedItem = $scope.city;
	      	console.log("geo city" , $scope.selectedItem );
      			
      		eventService.getCityEvents_past($scope.selectedItem).then(function(response){
			console.log("Selected City" , $scope.selectedItem);
			$scope.eventlist = response.data;
			
		    console.log("Controller Events " , response.data);
		}, function(err){
			console.log(err);
		});
      	//console.log("Events" , eventlist);
      }, function(err){
			console.log(err);
		});
    }, function(err) {
      // error
    });

    
});
})();

