(function(){

	var app = angular.module('starter.services');

	app.service('NewsService',function($http, API_URL){

		var self=this; 

		 var	urlbase = API_URL;

     var page = 1;
		var getNews = function(){

			return $http.get(urlbase+'/getNews/'+page).
            success(function(response, status, headers, config) {
            	var getNews = response.data;
            	 console.log("Service NewsList " +response.data);

		}).
          error(function(data) {
                    alert("Sorry, could not load the latest updates. Please try again later.");
         });
      };

      
      var loadmore1 = function(){

      return $http.get(urlbase+'/getNews/'+page).
            success(function(response, status, headers, config) {
              page++;
               console.log("Service NewsList " +response.data);

      }).
          error(function(data) {
                    alert("Sorry, could not load the latest updates. Please try again later.");
         });
      };

      var getNewsDetails = function(news_id){  

			return $http.post(urlbase+'/getNewsDetails', {'id' : news_id} ).
            success(function(response, status, headers, config) {
            	var NewsDetails = response.data;
            	console.log("Service NewsDetails" +response.data);

		}).
          error(function(data) {
                    //alert("Sorry, could not load the article. Please try again later.");
         });
      }
	return {
			getNews: getNews,
			 getNewsDetails: getNewsDetails
		};

	});
})();