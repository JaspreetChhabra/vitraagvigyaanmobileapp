(function(){
	var app=angular.module('starter.controllers');

	app.controller("NewsCtrl", function($scope, NewsService, $q, $http, moment, API_URL){

		//sideMenuService.openSideMenu('sideMenu');

		var self = this; 
		$scope.newslist = [];

		var	urlbase = API_URL;
	 	var in_progress = false;
		var page = 1;
		$scope.moredata = false;
		$scope.loaded = false;
		$scope.error = false;

		// $scope.refresh = function(){
		// 	in_progress = true;

		// 	if($scope.newslist.length == 0){
		// 		page = 1;
		// 		$scope.loadMore();
		// 	}
		// 	else{
		// 		//get the first news' item's published date
		// 		var published_date = $scope.newslist[0].published_date;
		// 		var payload = { published_date: published_date};

		// 		$http.post(urlbase + '/getLatest', payload).success(function(items) {
		// 			if(items === "\"\""){
		// 				return;
		// 			}
		// 			else if(items.status === "Error"){
		// 				alert("Sorry an error occurred. Please try again later.")
		// 				return;
		// 			}
					
		// 			//Push the items to the top of the list
		// 			for(var i = 0; i < items.length ; i++){
		// 				$scope.newslist.unshift(items[i]);
		// 			}
		// 		})
		// 		.error(function(){
		// 			alert("Sorry an error occurred. Please try again later.")
		// 		})
		// 	}


			
		// }
		
		$scope.loadMore = function() {
			if(in_progress) { $scope.$broadcast('scroll.infiniteScrollComplete'); return; }
			if($scope.moredata) { $scope.$broadcast('scroll.infiniteScrollComplete'); return; }

			in_progress = true;

	    	$http.get(urlbase + 'getNews/' + page).success(function(items) {
	    		$scope.loaded = true;
	    		page++;
	    		in_progress = false;

	    		if(items === "\"\""){ $scope.moredata = true; $scope.$broadcast('scroll.infiniteScrollComplete'); return; }

	    		if($scope.newslist.length == 0)
	    		{
	    			$scope.newslist = items;
	    			return;
	    		}
	    		else{
	    			angular.forEach(items, function(news) {
					  this.push(news);
					}, $scope.newslist);
	    		}

		    	if(items.length <= 6)
		    	{
					$scope.moredata = true;
				}

	        	$scope.$broadcast('scroll.infiniteScrollComplete');

	    	})
	    	.error(function(data){
	    		$scope.error = true;
				$scope.error_message = "Sorry couldn't load the article. Please try again later or check your internet connection."
	    	})
		};

		$scope.$on('$stateChangeSuccess', function() {
			$scope.loadMore();
		});

		$scope.$on('refresh', function() {
			$scope.refresh();
		});

	});

})();


