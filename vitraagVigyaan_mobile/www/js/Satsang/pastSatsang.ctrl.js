(function(){
	var app=angular.module('starter.controllers');

	app.controller("PastSatsangCtrl", function($scope, SatsangService, $q, $http ,$cordovaGeolocation, IMAGE_URL, Toast, $stateParams){

		var self=this; 

		$scope.satsangList =[];
		$scope.error = false;
		$scope.loaded = false;
		
		$scope.getPastSatsang = function()
		{
			var success_callback = function(response){
				console.log('response' , response);
				response = response.data;
				console.log('responseData' , response);

				$scope.loaded = true;

				if(response.status === "error"){
      				Toast.show(response.message);
      				$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't load the notification. Please try again later or check your internet connection.";
      				return;
      			}
      			
      			if(response.status === "success" && response.data === "[zero]"){
					$scope.eventlist = [];      				
      				Toast.show(response.message);
      				return;
      			}

				$scope.satsangList =[];
				$scope.satsangList = response.data;
			}

			var error_callback = function(err){
					Toast.show('api-error');
					$scope.error = true;
					$scope.loaded = true;
      				$scope.message = "Sorry couldn't load the notification. Please try again later or check your internet connection.";
			}

			SatsangService.getpastSatsang().then(success_callback , error_callback);
		}

		$scope.getPastSatsang();
	});
})();

