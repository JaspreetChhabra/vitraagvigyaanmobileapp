(function(){

	var app = angular.module('starter.services');

	app.service('SatsangService',function($http , API_URL){

	var self=this; 

	var	urlbase = API_URL;

    var _getliveSatsang = function(){
			return $http.get(urlbase+'live_satsang');
    };

    var _getpastSatsang = function(){
      return $http.get(urlbase+'past_satsang');
    };

    var _getpastSatsangDeatils = function(Satsangid){
      return $http.post(urlbase+'pastSatangDetails', {'id' : Satsangid} )
    };

    var _getannouncements = function(){
      return $http.get(urlbase+'satsang_announcements')
    };

	return {
			getliveSatsang: _getliveSatsang,
			getpastSatsang: _getpastSatsang,
	        getpastSatsangDeatils: _getpastSatsangDeatils,
	        getannouncements: _getannouncements
	       };
	   });
})();