(function(){
	var app=angular.module('starter.controllers');

	app.controller("SatsangCtrl", function($rootScope, $scope, SatsangService, $q, $http ,Toast, $stateParams,$localstorage,API_URL,$interval){

		$scope.type = '';
		$scope.satsang = '';

		var self=this; 

		$scope.satsangList =[];
		$scope.announcements_list =[];
		$scope.error = false;
		$scope.loaded = false;
		$scope.satsang_question = '';

		var session = $localstorage.getObject('session');
        var session_id = session.id;
        // console.log('session' , session_id);

		$scope.getSatsang = function()
		{
			var success_callback = function(response){
				
				response = response.data;
				//console.log('responseData' , response);

				$scope.loaded = true;

				if(response == '' || response == null){
					$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
      				return;
				}
				
				if(response.status === "error"){
      				Toast.show(response.message);
      				$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
      				return;
      			}
      			
      			if(response.status === "success" && response.data === "[zero]"){
					$scope.satsang = '';      
					$scope.loaded = true;				
      				Toast.show(response.message);
      				return;
      			}

				$scope.satsang ='';
				$scope.satsang = response.data;
				$scope.announcements_list = response.data.Announcement;
				console.log($scope.announcements_list);
			}

			var error_callback = function(err){
					Toast.show('api-error');
					$scope.error = true;
					$scope.loaded=true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
			}

			$scope.type = $stateParams.type;
			$scope.title = $scope.type + ' Satsang';

			if($scope.type === '' || $scope.type === undefined)
			{
				Toast.show('Sorry, an error occured. Please select a satsang or check your internet connection.');
				return;
			}

			if($scope.type === 'Live')
			{
				SatsangService.getliveSatsang().then(success_callback , error_callback);
			}
			else if($scope.type === 'Past')
			{
				SatsangService.getpastSatsangDeatils($stateParams.id).then(success_callback , error_callback);
			}
			else
			{
				Toast.show('Sorry, an error occured. Please select a satsang or check your internet connection.');
				return;
			}	
		}

		$scope.getSatsang();

		$scope.askquestion = function(ask_question)
		{

			var question = ask_question;
			console.log({Question : question , memberId : session_id});
            $http.post(API_URL + 'askquestion', {Question : question , memberId : session_id}).
	            success(function(data, status, headers, config)
	            {
	              
	                if(data.status=="success" && data.data === 0)
	                {
	                    Toast.show(data.message);
	                }
	                else if(data.status=="success" && data.data === 1)
	                {
	                    Toast.show(data.message);
	                }
	                else
	                {
	                	Toast.show('Sorry, an error occured');
	                }
	            }).
	            
	            error(function(data, status, headers, config) {
	                Toast.show("app-error");
					$scope.satsang_question = '';
	            });
		}

			$scope.getAnouncements = function()
			{
				$rootScope.$broadcast('loading:hide');
				var success_callback = function(response){
					response = response.data;
					//console.log('responseData' , response);

					if(response == '' || response == null){
						//Toast.show('Sorry, couldnt load the announcements.');
	      				return;
					}
					
					if(response.status === "error"){
	      				//Toast.show('Sorry, couldnt load the announcements.');
	      				return;
	      			}
	      			
	      			if(response.status === "success" && response.data === "[zero]"){			
	      				//Toast.show(response.message);
	      				return;
	      			}

					$scope.announcements_list = response.data;
					console.log($scope.announcements_list);
				}

				var error_callback = function(err){
						//Toast.show('api-error');
						return;
				}

				SatsangService.getannouncements().then(success_callback , error_callback);
				//$interval($scope.getAnouncements(), 500000);


		}

		if($scope.type === 'Live'){
			$interval( function(){ $scope.getAnouncements(); $rootScope.$broadcast('loading:hide');}, 10000);
		}
		else{
			return;
		}
	});
})();

