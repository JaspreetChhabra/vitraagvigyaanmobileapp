(function(){
	var app = angular.module('starter.controllers');

  app.controller("ContactUsCtrl", function($scope, uiGmapGoogleMapApi ,$stateParams , $http, $location , ContactService , Toast,
    $ionicLoading) {

    

    uiGmapGoogleMapApi.then(function(maps) {

      $scope.user = {};
      
      $scope.back = function(){
          console.log("hello!");
          $ionicHistory.goBack();
        }
        
      $scope.map = { center: { latitude:19.080111, longitude:72.909675}, zoom: 8 };

      $scope.marker = {
          id: 0,
          coords: {
            latitude:  19.080111,
            longitude:  72.909675
          },
          options: { 
            draggable: false,
            labelContent: '<h5>' + 'Vitraag Vigyaan' + '</h5>',
            labelClass: 'google-map-marker-label'
          },
          events: {
          }
        };
    });

    $scope.send_Message = function() {
      var user = $scope.user;
      //alert(user.first_name);
      ContactService.send_mail(user, function(err, response){
        Toast.show(response.message);
      });
    }
});

})();
   