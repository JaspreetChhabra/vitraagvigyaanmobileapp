(function(){

	var app = angular.module('starter.services');

	app.service('ContactService',function($http , API_URL){

		var self=this; 

		var	urlbase = API_URL;

    var send_mail = function(user , callback){
      console.log("register in service");   
      return $http.post(urlbase+'contact_mail', user ).
      success(function(response, status, headers, config) {

       if(status=="success"){
        var send_mail = response;
        callback(false, response);
      }
      else{
        var send_mail = response;
        callback(true, response);
      }

    }).
      error(function(data, status, headers, config) {
        callback(true, 'app-error');
      });
    }
    return {
     send_mail: send_mail
   };

 });
})();