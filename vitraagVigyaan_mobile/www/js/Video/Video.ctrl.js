(function(){
	var app=angular.module('starter.controllers');

	app.controller("VideoCtrl", function($scope, VideoService, $q, Toast){

		var self=this; 

		$scope.video =[];
		$scope.error = false;
		$scope.loaded = false;
		
		$scope.getVideos = function()
		{
			var success_callback = function(response){
				console.log('response' , response);
				response = response.data;
				console.log('responseData' , response);

				$scope.loaded = true;

				if(response.status === "error"){
      				Toast.show(response.message);
      				$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't the videos. Please try again later or check your internet connection.";
      				return;
      			}
      			
      			if(response.status === "success" && response.data === "[zero]"){
					$scope.video = [];      				
      				Toast.show(response.message);
      				return;
      			}

				$scope.video =[];
				$scope.video = response.data;
			}

			var error_callback = function(err){
					Toast.show('api-error');
					$scope.error = true;
					$scope.loaded = true;
      				$scope.message = "Sorry couldn't the videos. Please try again later or check your internet connection.";
			}

			VideoService.getVideos().then(success_callback , error_callback);
		}

		$scope.getVideos();
	});
})();

