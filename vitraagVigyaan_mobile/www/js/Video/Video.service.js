(function(){

	var app = angular.module('starter.services');

	app.service('VideoService',function($http , API_URL){

		var self=this; 

		var	urlbase = API_URL;

	var getVideos = function(){
			return $http.get(urlbase+'getAllVideos');
    };

    var getVideoDetails = function(videoId){
			return $http.post(urlbase+'getVideo' , {id: videoId});
    };

    
	return {
			 getVideos: getVideos,
			 getVideoDetails: getVideoDetails
		};

	});
})();