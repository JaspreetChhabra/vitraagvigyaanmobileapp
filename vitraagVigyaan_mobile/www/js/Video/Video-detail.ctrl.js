(function(){
	var app=angular.module('starter.controllers');

	app.controller("VideoDetailsCtrl", function($rootScope, $scope, VideoService, $q, $http ,Toast, $stateParams,API_URL){

		var self=this; 

		$scope.video =[];
		
		$scope.error = false;
		$scope.loaded = false;
		console.log($stateParams.id);
		$scope.getVideoDetail = function()
		{
			var success_callback = function(response){
				
				response = response.data;
				console.log('responseData' , response);

				$scope.loaded = true;

				if(response == '' || response == null){
					$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
      				return;
				}
				
				if(response.status === "error"){
      				Toast.show(response.message);
      				$scope.loaded = true;
      				$scope.error = true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
      				return;
      			}
      			
      			if(response.status === "success" && response.data === "[zero]"){
					$scope.satsang = '';      
					$scope.loaded = true;				
      				Toast.show(response.message);
      				return;
      			}

				$scope.video ='';
				$scope.video = response.data;
			}

			var error_callback = function(err){
					Toast.show('api-error');
					$scope.error = true;
					$scope.loaded=true;
      				$scope.message = "Sorry couldn't load the video. Please try again later or check your internet connection.";
			}

			
			if($stateParams.id === '' || $stateParams.id === undefined)
			{
				Toast.show('Sorry, an error occured. Please select a video or check your internet connection.');
				return;
			}

			
				VideoService.getVideoDetails($stateParams.id).then(success_callback , error_callback);
		}

		$scope.getVideoDetail();

		
	});
})();

